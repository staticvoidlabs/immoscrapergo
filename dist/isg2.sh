#!/bin/bash

echo "isg2.service: ## Starting ##" | systemd-cat -p info

cd /home/alex/bin/isg2
./immoscrapergo

echo "isg2.service: Shutting down." | systemd-cat -p info
