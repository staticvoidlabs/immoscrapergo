#!/bin/bash

echo "isg.service: ## Starting ##" | systemd-cat -p info

cd /home/alex/bin/isg
./immoscrapergo

echo "isg.service: Shutting down." | systemd-cat -p info
