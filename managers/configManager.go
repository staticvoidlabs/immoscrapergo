package managers

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/staticvoidlabs/immoscrapergo/models"
)

var mCurrentConfig models.Config
var mCurrentNotifyAgent models.NotifyAgent

// Public functions.
func ProcessConfigFile() {

	var currentConfig models.Config

	configFile, err := os.Open("./config.json")
	defer configFile.Close()

	if err != nil {
		fmt.Println(err.Error())
	}

	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&currentConfig)

	mCurrentConfig = currentConfig
}

func ProcessNotifyAgentFile() {

	var currentNotifyAgent models.NotifyAgent

	notifyAgentFile, err := os.Open("./notify.json")
	defer notifyAgentFile.Close()

	if err != nil {
		fmt.Println(err.Error())
	}

	jsonParser := json.NewDecoder(notifyAgentFile)
	jsonParser.Decode(&currentNotifyAgent)

	mCurrentNotifyAgent = currentNotifyAgent
}

func GetVersionInfo() string {

	tmpVersionInfo := "n/a"

	if mCurrentConfig.Version != "" {
		tmpVersionInfo = mCurrentConfig.Version
	}

	return tmpVersionInfo
}

func GetRefrehInterval() int32 {

	return mCurrentConfig.RefreshInterval
}
