package managers

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/smtp"
	"strings"
)

func SendMessage(jobIndex int, messageType string, matchedCriteria string, realestateLink string) {

	if messageType == "sms" {
		sendMessageSMS(jobIndex, matchedCriteria, realestateLink)
	} else if messageType == "email" {
		sendMessageEMAIL(jobIndex, matchedCriteria, realestateLink)
	} else {
		fmt.Println("No messageType given!")
	}

}

func sendMessageEMAIL(jobIndex int, matchedCriteria string, realestateLink string) {

	from := mCurrentNotifyAgent.Jobs[jobIndex].EmailUser
	password := mCurrentNotifyAgent.Jobs[jobIndex].EmailPass
	to := []string{mCurrentNotifyAgent.Jobs[jobIndex].RecipientEMAIL}
	smtpHost := mCurrentNotifyAgent.Jobs[jobIndex].EmailSMTPServer
	smtpPort := mCurrentNotifyAgent.Jobs[jobIndex].EmailSMTPPort

	message := []byte("Subject: Neuer Suchtreffer\r\n\r\n" + realestateLink + "\r\n\r\n" + matchedCriteria)

	// Create authentication
	auth := smtp.PlainAuth("", from, password, smtpHost)

	// Send actual message
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("EMAIL sent to " + mCurrentNotifyAgent.Jobs[jobIndex].RecipientEMAIL)
}

func sendMessageSMS(jobIndex int, matchedCriteria string, realestateLink string) {

	tmpMessageText := realestateLink + "\n" + matchedCriteria
	tmpMessageText = strings.ReplaceAll(tmpMessageText, " ", "%20")

	tmpURIString := mCurrentNotifyAgent.Jobs[jobIndex].SMS77Gateway + "?to=" + mCurrentNotifyAgent.Jobs[jobIndex].RecipientSMS + "&text=" + tmpMessageText + "&from=ImmoScraper"
	request, err := http.NewRequest("POST", tmpURIString, nil)
	request.Header.Set("X-Api-Key", mCurrentNotifyAgent.Jobs[jobIndex].SMS77APIKey)

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		fmt.Println(err)
	}
	defer response.Body.Close()

	if mCurrentConfig.DebugLevel == 1 {
		fmt.Println("response Status:", response.Status)
		fmt.Println("response Headers:", response.Header)
		body, _ := ioutil.ReadAll(response.Body)
		fmt.Println("response Body:", string(body))
	}

	fmt.Println("SMS sent to " + mCurrentNotifyAgent.Jobs[jobIndex].RecipientSMS)
}
