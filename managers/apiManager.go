package managers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/staticvoidlabs/immoscrapergo/models"
)

var mViewModelMainSite models.ViewModelMainSite

func InitAPIService() {

	WriteLogMessage("Starting Web API services...", true)

	// Create router instance.
	router := mux.NewRouter().StrictSlash(true)

	// Define routes and actions.
	router.HandleFunc("/", generateHTMLOutput)
	router.HandleFunc("/c", generateCustomHTMLOutput)
	router.HandleFunc("/immo/log", getStateInfoService)
	router.HandleFunc("/immo/ctrl/restart", restartSubsystem)

	// Prepare router to handle static files.
	fs := http.FileServer(http.Dir("./static/"))
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", fs))

	// Start rest service.
	tmpConfiguredApiPort := ":" + strconv.Itoa(mCurrentConfig.WebAPIPort)

	err := http.ListenAndServe(tmpConfiguredApiPort, router)

	if err != nil {
		fmt.Println("Error starting Web API services: ")
		fmt.Println(err)
	}

}

// Endpoint implementation.
func restartSubsystem(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:restartSubsystem", true)

	json.NewEncoder(w).Encode("ImmoScraperGoService_response:restart_subsystem:success.")
}

func getStateInfoService(w http.ResponseWriter, r *http.Request) {

	tmpLogMessages := GetCurrentLog()

	for _, message := range tmpLogMessages {
		w.Write([]byte(message + "\r\n"))
	}

}

func generateHTMLOutput(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:generateHTMLOutput", true)

	// Prepare datas ource
	prepareDataSource()

	// Now generate the HTML content.
	tmpTemplate, err := template.ParseFiles("static/index.html")

	if err != nil {
		json.NewEncoder(w).Encode("ImmoScraperGoService_response:generate_HTML_Output:error: " + err.Error())
	}

	mRealEstateRepo.LastWebAccess = time.Now()
	isFirstRun = false
	tmpTemplate.Execute(w, mViewModelMainSite)
}

func generateCustomHTMLOutput(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:generateCustomHTMLOutput", true)

	// Prepare datas ource
	prepareDataSource()

	// Now generate the HTML content.
	tmpTemplate, err := template.ParseFiles("static/indexc.html")

	if err != nil {
		json.NewEncoder(w).Encode("ImmoScraperGoService_response:generate_Custom_HTML_Output:error: " + err.Error())
	}

	mRealEstateRepo.LastWebAccess = time.Now()
	isFirstRun = false
	tmpTemplate.Execute(w, mViewModelMainSite)

}

func htmlTesting(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:testing", true)

	//json.NewEncoder(w).Encode("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
	fmt.Print("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
}

func prepareDataSource() {

	// Build data source object.
	mViewModelMainSite.SiteTitle = "ImmoScraperGo"

	/*
		if (mRealEstateRepo.NewItemsEKA > 0 || mRealEstateRepo.NewItemsIW > 0 || mRealEstateRepo.NewItemsIS24 > 0) && !isFirstRun {
			tmpString := strconv.Itoa(mRealEstateRepo.NewItemsEKA + mRealEstateRepo.NewItemsIW + mRealEstateRepo.NewItemsIS24)

			if tmpString == "1" {
				mViewModelMainSite.SiteTitle = tmpString + " neue Wohnung"
			} else {
				mViewModelMainSite.SiteTitle = tmpString + " neue Wohnungen"
			}

		} else if mRealEstateRepo.NewItemsEKA == 0 && mRealEstateRepo.NewItemsIW == 0 && mRealEstateRepo.NewItemsIS24 == 0 && mViewModelMainSite.SiteTitle != "ImmoScraperGo" {
			mViewModelMainSite.SiteTitle = "Neue Wohnungen"
		} else {
			mViewModelMainSite.SiteTitle = "ImmoScraperGo"
		}
	*/

	mViewModelMainSite.RealEstatesEKA = mRealEstateRepo.ItemsEKA
	mViewModelMainSite.RealEstatesIW = mRealEstateRepo.ItemsIW
	mViewModelMainSite.RealEstatesIS24 = mRealEstateRepo.ItemsIS24
	mViewModelMainSite.RealEstatesOM = mRealEstateRepo.ItemsOM
	mViewModelMainSite.RealEstatesSAGA = mRealEstateRepo.ItemsSAGA
	mViewModelMainSite.RealEstatesBUWOG = mRealEstateRepo.ItemsBUWOG
	/*
		mViewModelMainSite.NewItemsEKA = mRealEstateRepo.NewItemsEKA
		mViewModelMainSite.NewItemsIW = mRealEstateRepo.NewItemsIW
		mViewModelMainSite.NewItemsIS24 = mRealEstateRepo.NewItemsIS24
	*/
	mViewModelMainSite.NewItemsEKA = len(mRealEstateRepo.ItemsEKA)
	mViewModelMainSite.NewItemsIW = len(mRealEstateRepo.ItemsIW)
	mViewModelMainSite.NewItemsIS24 = len(mRealEstateRepo.ItemsIS24)
	mViewModelMainSite.NewItemsOM = len(mRealEstateRepo.ItemsOM)
	mViewModelMainSite.NewItemsSAGA = len(mRealEstateRepo.ItemsSAGA)
	mViewModelMainSite.NewItemsBUWOG = len(mRealEstateRepo.ItemsBUWOG)
	mViewModelMainSite.RefreshedAtEKA = mRealEstateRepo.LastRefreshEKA
	mViewModelMainSite.RefreshedAtIW = mRealEstateRepo.LastRefreshIW
	mViewModelMainSite.RefreshedAtIS24 = mRealEstateRepo.LastRefreshIS24
	mViewModelMainSite.RefreshedAtOM = mRealEstateRepo.LastRefreshOM
	mViewModelMainSite.RefreshedAtBUWOG = mRealEstateRepo.LastRefreshBUWOG
	mViewModelMainSite.RefreshedAtOM = mRealEstateRepo.LastRefreshOM
	mViewModelMainSite.ApartmentRequestText = getApartmentRequestText(1)
	mViewModelMainSite.ApartmentRequestText2 = getApartmentRequestText(2)

}
