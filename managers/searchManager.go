package managers

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/tidwall/gjson"
	"gitlab.com/staticvoidlabs/immoscrapergo/models"
)

var isFirstRun bool = true
var ShouldExit bool = false
var mRealEstateRepo models.RealEstateRepo

func StartSearchLoop() {

	go getImmoObjectsSAGA()
	go getImmoObjectsIW()
	go getImmoObjectsIS24()
	go getImmoObjectsOM()
	go getImmoObjectsEKA()

	go getImmoObjectsBUWOG()

}

func getImmoObjectsSAGA() {

	fmt.Println("Refreshing SAGA objects.")

	tmpCounterNewSAGA := 0

	// Request the HTML page.
	res, err := http.Get(mCurrentConfig.UrlSAGA)
	if err != nil {
		fmt.Println(err)
	}
	if res.StatusCode != 200 {
		fmt.Println("Failed to get SAGA data: " + res.Status)
	}
	defer res.Body.Close()

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Println(err)
	}

	var tmpRealEstateItems []models.RealEstateItem

	// Find the review items
	doc.Find(".teaser3--listing").Each(func(i int, s *goquery.Selection) {

		var tmpRealEstateItem models.RealEstateItem

		tmpRealEstateItem.TimeStamp = strings.TrimSpace(s.Find(".aditem-main--top--right").Text())
		//tmpRealEstateItem.Location = strings.TrimSpace(s.Find(".aditem-main--top--left").Text())
		tmpRealEstateItem.Title = strings.TrimSpace(s.Find(".h3").Text())
		getLocationSAGA(s, &tmpRealEstateItem)
		tmpRealEstateItem.Id = generateID(tmpRealEstateItem.TimeStamp, tmpRealEstateItem.Price, tmpRealEstateItem.Location)
		tmpRealEstateItem.Link = "https://www.saga.hamburg" + strings.TrimSpace(s.Find(".inner").AttrOr("href", "none"))
		tmpRealEstateItem.ImageURL = "https://www.saga.hamburg" + strings.TrimSpace(s.Find(".media__img").AttrOr("src", "none"))
		tmpRealEstateItem.Rooms = "n/a"
		tmpRealEstateItem.Area = "n/a"
		tmpRealEstateItem.Price = "n/a"
		tmpRealEstateItem.ImageOrigin = "static/noimage.png"

		if mCurrentConfig.DebugLevel == 1 {
			fmt.Println(tmpRealEstateItem.TimeStamp + " | " + tmpRealEstateItem.Price + " | " + tmpRealEstateItem.Location)
			fmt.Println(tmpRealEstateItem.Title + " (" + tmpRealEstateItem.Id + ")")
			fmt.Println(tmpRealEstateItem.Link)
			fmt.Println(tmpRealEstateItem.ImageURL)
			fmt.Println("")
		}

		tmpIsItemNew := isItemNew(tmpRealEstateItem.Id)
		if tmpIsItemNew {
			tmpCounterNewSAGA++
		}

		tmpRealEstateItems = append(tmpRealEstateItems, tmpRealEstateItem)

	})

	mRealEstateRepo.ItemsSAGA = nil
	mRealEstateRepo.ItemsSAGA = tmpRealEstateItems
	mRealEstateRepo.NewItemsSAGA = tmpCounterNewSAGA
	tmpCounterNewSAGA = 0
	mRealEstateRepo.LastRefreshSAGA = time.Now()
	fmt.Println("Refehed SAGA at: " + mRealEstateRepo.LastRefreshIW.String())

	go checkAlerts(mRealEstateRepo.ItemsSAGA) // Find the review items
}

func getImmoObjectsBUWOG() {

	fmt.Println("Refreshing BUWOG objects.")

	tmpCounterNewBUWOG := 0

	// Request the HTML page.
	res, err := http.Get(mCurrentConfig.UrlBUWOG)
	if err != nil {
		fmt.Println(err)
	}
	if res.StatusCode != 200 {
		fmt.Println("Failed to get BUWOG data: " + res.Status)
	}
	defer res.Body.Close()

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Println(err)
	}

	var tmpRealEstateItems []models.RealEstateItem

	// Find the review items
	doc.Find("article").Each(func(i int, s *goquery.Selection) {

		// TODO

	})

	mRealEstateRepo.ItemsBUWOG = nil
	mRealEstateRepo.ItemsBUWOG = tmpRealEstateItems
	mRealEstateRepo.NewItemsBUWOG = tmpCounterNewBUWOG
	tmpCounterNewBUWOG = 0
	mRealEstateRepo.LastRefreshBUWOG = time.Now()
	fmt.Println("Refehed BUWOG at: " + mRealEstateRepo.LastRefreshIW.String())

	go checkAlerts(mRealEstateRepo.ItemsBUWOG)
}

func getImmoObjectsEKA() {

	fmt.Println("Refreshing Ebaykleinanzeigen.de objects.")

	tmpCounterNewEKA := 0

	// Request the HTML page...

	// the old way.
	//res, err := http.Get(mCurrentConfig.UrlEKA)

	// the new experimental way.
	client := &http.Client{
		//CheckRedirect: redirectPolicyFunc,
	}
	req, err := http.NewRequest("GET", mCurrentConfig.UrlEKA, nil)
	req.Header.Add("Accept", `text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8`)
	req.Header.Add("User-Agent", `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11`)
	res, err := client.Do(req)
	//fmt.Println(res, err)

	if err != nil {
		fmt.Println(err)
	}
	if res.StatusCode != 200 {
		fmt.Println("Failed to get EKA data: " + res.Status)
	}
	defer res.Body.Close()

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Println(err)
	}

	var tmpRealEstateItems []models.RealEstateItem

	// Find the review items
	doc.Find("article").Each(func(i int, s *goquery.Selection) {

		var tmpRealEstateItem models.RealEstateItem

		tmpRealEstateItem.TimeStamp = strings.TrimSpace(s.Find(".aditem-main--top--right").Text())
		tmpRealEstateItem.Location = strings.TrimSpace(s.Find(".aditem-main--top--left").Text())
		tmpRealEstateItem.Title = strings.TrimSpace(s.Find(".ellipsis").Text())
		tmpRealEstateItem.Price = strings.TrimSpace(s.Find(".aditem-main--middle--price-shipping--price").Text())
		tmpRealEstateItem.Rooms = getRoomsEKA(s)
		tmpRealEstateItem.Area = getAreaEKA(s)
		tmpRealEstateItem.Id = generateID(tmpRealEstateItem.TimeStamp, tmpRealEstateItem.Price, tmpRealEstateItem.Location)
		tmpRealEstateItem.Link = "https://www.ebay-kleinanzeigen.de" + strings.TrimSpace(s.Find(".ellipsis").AttrOr("href", "none"))
		//tmpRealEstateItem.ImageURL = getImageLink(strings.TrimSpace(s.Find(".imagebox").AttrOr("data-imgsrc", "none")))
		tmpRealEstateItem.ImageURL = getImageLink(strings.TrimSpace(s.Find("img").AttrOr("src", "none")))

		tmpRealEstateItem.ImageOrigin = "static/EKA.png"

		if mCurrentConfig.DebugLevel == 1 {
			fmt.Println(tmpRealEstateItem.TimeStamp + " | " + tmpRealEstateItem.Price + " | " + tmpRealEstateItem.Location)
			fmt.Println(tmpRealEstateItem.Title + " (" + tmpRealEstateItem.Id + ")")
			fmt.Println(tmpRealEstateItem.Link)
			fmt.Println(tmpRealEstateItem.ImageURL)
			fmt.Println("")
		}

		tmpIsItemNew := isItemNew(tmpRealEstateItem.Id)
		if tmpIsItemNew {
			tmpCounterNewEKA++
		}

		tmpRealEstateItems = append(tmpRealEstateItems, tmpRealEstateItem)
	})

	mRealEstateRepo.ItemsEKA = nil
	mRealEstateRepo.ItemsEKA = tmpRealEstateItems
	mRealEstateRepo.NewItemsEKA = tmpCounterNewEKA
	tmpCounterNewEKA = 0
	mRealEstateRepo.LastRefreshEKA = time.Now()
	fmt.Println("Refehed EKA at: " + mRealEstateRepo.LastRefreshIW.String())

	go checkAlerts(mRealEstateRepo.ItemsEKA)
}

func getImageLink(jsonResultListEntries string) string {

	tmpImageink := "static/noimage.png"

	if jsonResultListEntries != "" && jsonResultListEntries != "none" {
		tmpImageink = jsonResultListEntries
	}

	return tmpImageink
}

func getValueForKey(jsonString string, key string) string {

	tmpValue1 := gjson.Get(jsonString, key)

	return tmpValue1.String()
}

func getIS24ImageLink(jsonString string, key string) string {

	tmpValue1 := gjson.Get(jsonString, key)

	tmpLinkStrings := strings.Split(tmpValue1.String(), "/ORIG/")

	if len(tmpLinkStrings) > 1 {
		return tmpLinkStrings[0]
	} else {
		return ""
	}

}

func setIS24TimeStamps(jsonString string, key string, realEstateItem *models.RealEstateItem) {

	tmpTimeStamp := gjson.Get(jsonString, key).String()

	tmpTimeStamp = strings.Split(tmpTimeStamp, ".")[0]
	tmpTimeStamp = strings.Replace(tmpTimeStamp, "T", " ", 1)

	myDate, err := time.Parse("2006-01-02 15:04:05", tmpTimeStamp)
	if err != nil {
		fmt.Println(err)
	}

	realEstateItem.TimeStampDT = myDate
	realEstateItem.TimeStamp = tmpTimeStamp
}

func getIS24Location(jsonString string, keyBase string) string {

	tmpPostalCode := gjson.Get(jsonString, keyBase+".realEstateEntry.address.postcode")
	tmpCityAndQuarter := gjson.Get(jsonString, keyBase+".realEstateEntry.address.description.text")

	return tmpPostalCode.String() + " " + tmpCityAndQuarter.String()
}

func getImmoObjectsOM() {

	fmt.Println("Refreshing ohne-makler.net objects.")

	tmpCounterNewOM := 0

	// Request the HTML page.
	res, err := http.Get(mCurrentConfig.UrlOM)
	if err != nil {
		fmt.Println(err)
	}
	if res.StatusCode != 200 {
		fmt.Println("Failed to get IS24 data: " + res.Status)
	}
	defer res.Body.Close()

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Println(err)
	}

	var tmpRealEstateItems []models.RealEstateItem

	// Find the review items
	doc.Find("TR").Each(func(i int, s *goquery.Selection) {

		tmpTitle := strings.TrimSpace(s.Find(".red").Text())
		tmpTitle = strings.ReplaceAll(tmpTitle, "\t", "")
		tmpTitle = strings.ReplaceAll(tmpTitle, "\n", "")

		if tmpTitle != "" {

			var tmpRealEstateItem models.RealEstateItem

			tmpRealEstateItem.Title = strings.TrimSpace(tmpTitle)
			tmpRealEstateItem.Link = "https://www.ohne-makler.net" + strings.TrimSpace(s.Find(".red").AttrOr("href", "none"))
			tmpRealEstateItem.ImageURL = "https://www.ohne-makler.net" + strings.TrimSpace(s.Find("img").AttrOr("src", "none"))

			var index int = 0
			s.Find("TD").Each(func(i int, subSelection *goquery.Selection) {

				if index == 1 {
					tmpText := subSelection.Text()
					tmpText = strings.ReplaceAll(tmpText, "\t", "")
					tmpText = strings.ReplaceAll(tmpText, "\n\n", "")
					tmpText = strings.ReplaceAll(tmpText, "\n\n\n", "")
					tmpText = strings.ReplaceAll(tmpText, "Objekt-Nr", "_____Objekt-Nr")
					tmpText = strings.ReplaceAll(tmpText, "Zimmer:", "_____Zimmer:")
					tmpText = strings.ReplaceAll(tmpText, "\n", "_____")

					tmpTextArray := strings.Split(tmpText, "_____")

					for _, e := range tmpTextArray {

						if e != "" {

							tmpTextCleanArray := strings.Split(strings.TrimSpace(e), ":")

							if len(tmpTextCleanArray) == 2 && tmpTextCleanArray[0] == "Objekt-Nr." {
								tmpRealEstateItem.Id = tmpTextCleanArray[1]
							} else if len(tmpTextCleanArray) == 2 && tmpTextCleanArray[0] == "Adresse" {
								tmpRealEstateItem.Location = getDistrictByPostalCode(strings.TrimSpace(tmpTextCleanArray[1]))
							} else if len(tmpTextCleanArray) == 2 && tmpTextCleanArray[0] == "Zimmer" {
								tmpRealEstateItem.Rooms = tmpTextCleanArray[1]
							} else if len(tmpTextCleanArray) == 2 && tmpTextCleanArray[0] == "Wohnfläche" {
								tmpRealEstateItem.Area = tmpTextCleanArray[1]
							}

						}
					}

				} else if index == 2 {
					tmpText2 := subSelection.Text()
					tmpText2 = strings.ReplaceAll(tmpText2, "\t", "")
					tmpText2 = strings.ReplaceAll(tmpText2, "\n", "")

					tmpTextCeanArray2 := strings.Split(strings.TrimSpace(tmpText2), " €")

					if len(tmpTextCeanArray2) == 2 && (strings.Contains(tmpTextCeanArray2[1], "Miete") || strings.Contains(tmpTextCeanArray2[1], "Kaufpreis")) {
						tmpRealEstateItem.Price = tmpTextCeanArray2[0] + " €"
					}
				}

				index++

			})

			tmpRealEstateItem.ImageOrigin = "static/noimage.png"

			if mCurrentConfig.DebugLevel == 1 {
				fmt.Println(tmpRealEstateItem.TimeStamp + " | " + tmpRealEstateItem.Price + " | " + tmpRealEstateItem.Location)
				fmt.Println(tmpRealEstateItem.Title + " (" + tmpRealEstateItem.Id + ")")
				fmt.Println(tmpRealEstateItem.Link)
				fmt.Println(tmpRealEstateItem.ImageURL)
				fmt.Println("")
			}

			tmpIsItemNew := isItemNew(tmpRealEstateItem.Id)
			if tmpIsItemNew {
				tmpCounterNewOM++
			}

			tmpRealEstateItems = append(tmpRealEstateItems, tmpRealEstateItem)
		}

	})

	mRealEstateRepo.ItemsOM = nil
	mRealEstateRepo.ItemsOM = tmpRealEstateItems
	mRealEstateRepo.NewItemsOM = tmpCounterNewOM
	tmpCounterNewOM = 0
	mRealEstateRepo.LastRefreshOM = time.Now()
	fmt.Println("Refehed OM at: " + mRealEstateRepo.LastRefreshIW.String())

	go checkAlerts(mRealEstateRepo.ItemsOM)
}

func getDistrictByPostalCode(postalCode string) string {

	tmpReturnValue := ""

	tmpStringArray := strings.Split(postalCode, ",")
	tmpStringArray2 := strings.Split(tmpStringArray[len(tmpStringArray)-1], " ")

	tmpPostalCodeInt, _ := strconv.Atoi(tmpStringArray2[0])
	tmpReturnValue = mPostalCodeMap[tmpPostalCodeInt]

	return tmpReturnValue
}

func checkAlerts(realEstateItems []models.RealEstateItem) {

	for i, job := range mCurrentNotifyAgent.Jobs {

		var tmpShouldSend bool = false
		//var tmpAlreadySentMessage bool = false
		var tmpMatchedCriteria string = ""

		if job.IsActive {

			// Get IDs of previously sent messages.
			tmpIDs := getIDsOfSentMessages(i)

			// Get criterias to search for.
			tmpLocations := strings.Split(job.CriteriaLocation, ";")
			tmpRooms := strings.Split(job.CriteriaRooms, ";")
			tmpArea := job.CriteriaArea

			for _, e := range realEstateItems {

				if mCurrentConfig.DebugLevel == 4 {
					fmt.Println("Alert criteria check for ID " + e.Id + " >>> " + e.Location + " | " + e.Rooms + " | " + e.Area + " | " + e.Price)
				}

				tmpShouldSend = false
				tmpMatchedCriteria = ""

				// Check location criteria.
				for _, critLocation := range tmpLocations {
					if strings.Contains(e.Location, critLocation) {
						tmpShouldSend = true
						tmpMatchedCriteria = e.Location
						break
					}
				}

				// Check price criteria.
				if tmpShouldSend {
					tmpStringPriceCurrItem := strings.Split(e.Price, " ")
					tmpStringPriceCurrItem[0] = strings.Replace(tmpStringPriceCurrItem[0], ",", "", -1)
					tmpStringPriceCurrItem[0] = strings.Replace(tmpStringPriceCurrItem[0], ".", "", -1)

					tmpIntPriceCurrItem, _ := strconv.Atoi(tmpStringPriceCurrItem[0])
					tmpIntMaxPriceJob, _ := strconv.Atoi(job.CriteriaMaxPrice)

					if tmpIntPriceCurrItem <= tmpIntMaxPriceJob {
						tmpShouldSend = true
						tmpMatchedCriteria = tmpMatchedCriteria + " | " + e.Price
					}
				}

				// Check rooms criteria.
				if tmpShouldSend {
					for _, critRoom := range tmpRooms {
						if strings.Contains(e.Rooms, critRoom) {
							tmpShouldSend = true
							tmpMatchedCriteria = tmpMatchedCriteria + " | " + e.Rooms + " Zimmer"
							break
						}
					}
				}

				// Check area criteria.
				if tmpShouldSend {
					tmpStringAreaCrit := strings.Split(tmpArea, " ")

					tmpStringAreaCurrItem := strings.Split(strings.TrimSpace(e.Area), " ")
					tmpStringAreaCurrItem = strings.Split(tmpStringAreaCurrItem[0], ",")
					tmpStringAreaCurrItem = strings.Split(tmpStringAreaCurrItem[0], ".")

					tmpStringAreaCritInt, _ := strconv.Atoi(tmpStringAreaCrit[0])
					tmpStringAreaCurrItemInt, _ := strconv.Atoi(tmpStringAreaCurrItem[0])

					if tmpStringAreaCurrItemInt >= tmpStringAreaCritInt {

						tmpMatchedCriteria = tmpMatchedCriteria + " | " + tmpStringAreaCurrItem[0] + " m²"

						for _, i := range tmpIDs {
							if i == e.Id {
								tmpShouldSend = false
								break
							}
						}
						if tmpShouldSend {

							// This section is disabled to receive all messages.
							/*
								if !tmpAlreadySentMessage {

									fmt.Println("Sending message for ID: " + e.Id)
									SendMessage(i, job.MessageType, tmpMatchedCriteria, e.Link)
									tmpAlreadySentMessage = true

								} else {
									fmt.Println("Skipping message for ID: " + e.Id)
								}
							*/

							fmt.Println("Sending message for ID: " + e.Id)
							SendMessage(i, job.MessageType, tmpMatchedCriteria, e.Link)
							appendNotifyHistory(i, e.Id)
							tmpShouldSend = false
						}
						continue
					}
				}
			}
		}
	}
}

func getIDsOfSentMessages(jobIndex int) []string {

	var tmpIDs []string

	tmpIndexString := strconv.Itoa(jobIndex)

	f, err := os.Open("./sentmessages" + tmpIndexString + ".txt")
	if err != nil {
		fmt.Println(err)
		return tmpIDs
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		tmpIDs = append(tmpIDs, scanner.Text())
	}

	return tmpIDs
}

func appendNotifyHistory(jobIndex int, id string) {

	tmpIndexString := strconv.Itoa(jobIndex)

	f, err := os.OpenFile("./sentmessages"+tmpIndexString+".txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println(err)
	}

	defer f.Close()

	if _, err = f.WriteString(id + "\n"); err != nil {
		fmt.Println(err)
	}

}

func getImmoObjectsIW() {

	fmt.Println("Refreshing Immowelt.de objects.")

	tmpCounterNewIW := 0

	// Request the HTML page.
	res, err := http.Get(mCurrentConfig.UrlIW)
	if err != nil {
		fmt.Println(err)
	}
	if res.StatusCode != 200 {
		fmt.Println("Failed to get IS24 data: " + res.Status)
	}
	defer res.Body.Close()

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Println(err)
	}

	var tmpRealEstateItems []models.RealEstateItem

	// Find the review items
	doc.Find(".EstateItem-4409d").Each(func(i int, s *goquery.Selection) {

		var tmpRealEstateItem models.RealEstateItem

		tmpRealEstateItem.Id = strings.TrimSpace(s.Find("a").AttrOr("id", "none"))
		tmpRealEstateItem.Location = strings.Replace(strings.TrimSpace(s.Find(".IconFact-e8a23").First().Text()), "location", "", 1)
		tmpRealEstateItem.Rooms = getRoomsIW(s)
		tmpRealEstateItem.Area = getAreaIW(s)
		tmpRealEstateItem.Price = getPriceIW(s)
		tmpRealEstateItem.Title = strings.TrimSpace(s.Find("h2").Text())
		tmpRealEstateItem.Link = strings.TrimSpace(s.Find("a").AttrOr("href", "none"))
		tmpRealEstateItem.ImageURL = getImageLink(strings.TrimSpace(s.Find("img").AttrOr("data-src", "none")))
		tmpRealEstateItem.ImageOrigin = "static/IW.png"

		if mCurrentConfig.DebugLevel == 1 {
			fmt.Println(tmpRealEstateItem.Price + " | " + tmpRealEstateItem.Area + " | " + tmpRealEstateItem.Rooms + " | " + tmpRealEstateItem.Location)
			fmt.Println(tmpRealEstateItem.Title + " (" + tmpRealEstateItem.Id + ")")
			fmt.Println(tmpRealEstateItem.Link)
			fmt.Println(tmpRealEstateItem.ImageURL)
			fmt.Println("")
		}

		tmpIsItemNew := isItemNew(tmpRealEstateItem.Id)
		if tmpIsItemNew {
			tmpCounterNewIW++
		}

		tmpRealEstateItems = append(tmpRealEstateItems, tmpRealEstateItem)
	})

	mRealEstateRepo.ItemsIW = nil
	mRealEstateRepo.ItemsIW = tmpRealEstateItems
	mRealEstateRepo.NewItemsIW = tmpCounterNewIW
	tmpCounterNewIW = 0
	mRealEstateRepo.LastRefreshIW = time.Now()
	fmt.Println("Refehed IW at: " + mRealEstateRepo.LastRefreshIW.String())

	go checkAlerts(mRealEstateRepo.ItemsIW)
}

func isItemNew(id string) bool {

	for _, e := range mRealEstateRepo.ItemsEKA {
		if e.Id == id {
			return false
		}
	}

	for _, e := range mRealEstateRepo.ItemsIW {
		if e.Id == id {
			return false
		}
	}

	for _, e := range mRealEstateRepo.ItemsIS24 {
		if e.Id == id {
			return false
		}
	}

	return true
}

func getLocationSAGA(selection *goquery.Selection, item *models.RealEstateItem) {

	selection.Find(".hidden-phone").Each(func(i int, s *goquery.Selection) {

		tmpValue := s.Text()
		s1 := strings.Split(tmpValue, ": ")

		if len(s1) > 1 && s1[0] == "Straße" { // Check for location.
			item.Location = s1[1]
		}

	})

}

func getPriceAreaRoomsIS24(selection *goquery.Selection, item *models.RealEstateItem) {

	selection.Find("dd").Each(func(i int, s *goquery.Selection) {
		tmpValue := s.Text()
		s1 := strings.Split(tmpValue, " ")

		if len(s1) > 1 && s1[1] == "€" { // Check for price.
			item.Area = s1[0]
		} else if len(s1) > 1 && s1[1] == "m²" { // Check for area.
			item.Price = s1[0]
		} else { // Check for rooms.
			item.Rooms = s1[0]
		}

	})

}

func getAreaEKA(selection *goquery.Selection) string {

	tmpArea := ""

	selection.Find(".simpletag").Each(func(i int, s *goquery.Selection) {
		tmpValue := s.Text()
		s1 := strings.Split(tmpValue, " ")
		if len(s1) > 1 && s1[1] == "m²" {
			tmpArea = tmpValue
		}

	})

	return tmpArea
}

func getRoomsEKA(selection *goquery.Selection) string {

	tmpRooms := ""

	selection.Find(".simpletag").Each(func(i int, s *goquery.Selection) {
		tmpValue := s.Text()
		s1 := strings.Split(tmpValue, " ")
		if len(s1) > 1 && s1[1] == "Zimmer" {
			tmpRooms = tmpValue
		}

	})

	return tmpRooms
}

func getPriceIW(selection *goquery.Selection) string {

	tmpPrice := ""

	selection.Find("div").Each(func(i int, s *goquery.Selection) {
		tmpValue := s.AttrOr("data-test", "")
		if tmpPrice == "" && tmpValue == "price" {
			tmpPrice = strings.TrimSpace(s.Text())
		}

	})

	return tmpPrice
}

func getRoomsIW(selection *goquery.Selection) string {

	tmpRooms := ""

	selection.Find("div").Each(func(i int, s *goquery.Selection) {
		tmpValue := s.AttrOr("data-test", "")
		if tmpRooms == "" && tmpValue == "rooms" {
			tmpRooms = strings.TrimSpace(s.Text())
		}

	})

	return tmpRooms
}

func getAreaIW(selection *goquery.Selection) string {

	tmpArea := ""

	selection.Find("div").Each(func(i int, s *goquery.Selection) {
		tmpValue := s.AttrOr("data-test", "")
		if tmpArea == "" && tmpValue == "area" {
			tmpArea = strings.TrimSpace(s.Text())
		}

	})

	return tmpArea
}

func generateID(t1 string, t2 string, t3 string) string {

	tmpId := ""

	// TimeStamp
	s1 := strings.Split(t1, " ")
	if len(s1) == 2 {
		s2 := strings.Split(s1[1], ":")
		tmpId = s2[0] + s2[1]
	}

	// Price
	s3 := strings.Split(t2, " ")
	s4 := strings.Replace(s3[0], ".", "", 1)
	tmpId = tmpId + s4

	// Location
	s5 := strings.Split(t3, " ")
	tmpId = tmpId + s5[0]

	return tmpId
}

func getApartmentRequestText(id int) string {

	tmpApartmentRequestText := ""

	tmpFileName := ""

	if id == 1 {
		tmpFileName = "./TextAnfrage.txt"
	} else if id == 2 {
		tmpFileName = "./TextAnfrage2.txt"
	}

	f, err := os.Open(tmpFileName)
	if err != nil {
		fmt.Println(err)
		return "empty"
	}
	defer f.Close()

	var tmpLines []string
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		tmpLines = append(tmpLines, scanner.Text())
	}

	for _, line := range tmpLines {
		tmpApartmentRequestText = tmpApartmentRequestText + line + "\n"
	}

	if tmpApartmentRequestText == "" {
		return "empty"
	}

	return tmpApartmentRequestText
}

func getImmoObjectsIS24() {

	if !mCurrentConfig.DeactivateIS24 {

		fmt.Println("Refreshing ImmobilienScout24.de objects.")

		tmpCounterNewIS24 := 0

		/*
			// Request the HTML page. (GET)
			res, err := http.Get(mCurrentConfig.UrlIS24)
			if err != nil {
				fmt.Println(err)
			}
			defer res.Body.Close()
		*/

		// Request the HTML page. (POST)
		client := &http.Client{
			//CheckRedirect: redirectPolicyFunc,
		}
		req, err := http.NewRequest("GET", mCurrentConfig.UrlIS24, nil)
		req.Header.Add("Accept", `application/json,text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8`)
		req.Header.Add("User-Agent", `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11`)
		req.Header.Add("Cache-Control", "no-cache")
		res, err := client.Do(req)
		//fmt.Println(res, err)

		if err != nil {
			fmt.Println(err)
		}
		if res.StatusCode != 200 {
			fmt.Println("Failed to get IS24 data: " + res.Status)
		}
		defer res.Body.Close()

		// Load the HTML document (PLAIN)
		body, err := ioutil.ReadAll(res.Body)
		fmt.Println("Body:", string(body))

		tmpBodyLines := strings.Split(string(body), "\n")

		var tmpRealEstateItems []models.RealEstateItem

		if len(tmpBodyLines) == 1 {

			// Prepare body content.
			tmpJsonBody := tmpBodyLines[0]
			tmpJsonBody = strings.Replace(tmpJsonBody, "resultlist.resultlist", "resultlist", 1)
			tmpValue1 := gjson.Get(tmpJsonBody, "searchResponseModel.resultlist.resultlistEntries.0.resultlistEntry")

			// Prepare real estate entries.
			tmpJsonResultListEntries := tmpValue1.String()
			tmpJsonResultListEntries = strings.ReplaceAll(tmpJsonResultListEntries, "resultlist.realEstate", "realEstateEntry")
			tmpJsonResultListEntries = "{ \"realEstateEntries\" : " + tmpJsonResultListEntries + " }"
			tmpValue2 := gjson.Get(tmpJsonResultListEntries, "realEstateEntries")

			if mCurrentConfig.DebugLevel == 2 {
				fmt.Println("DEBUG >>> IS24 HTML body raw data:  " + tmpValue1.String())
				fmt.Println("DEBUG >>> IS24 HTML result list raw data:  " + tmpValue2.String())
			}

			// Process real estate entries.
			for i := 0; i < 14; i++ {
				tmpKeyString := "realEstateEntries." + strconv.Itoa(i) + ".realEstateEntry.title"
				tmpValue3 := gjson.Get(tmpJsonResultListEntries, tmpKeyString)

				if tmpValue3.String() != "" {
					var tmpRealEstateItem models.RealEstateItem

					setIS24TimeStamps(tmpJsonResultListEntries, "realEstateEntries."+strconv.Itoa(i)+".@modification", &tmpRealEstateItem)
					tmpRealEstateItem.Location = getIS24Location(tmpJsonResultListEntries, "realEstateEntries."+strconv.Itoa(i))
					tmpRealEstateItem.Title = getValueForKey(tmpJsonResultListEntries, "realEstateEntries."+strconv.Itoa(i)+".realEstateEntry.title")
					tmpRealEstateItem.Price = getValueForKey(tmpJsonResultListEntries, "realEstateEntries."+strconv.Itoa(i)+".attributes.0.attribute.0.value")
					tmpRealEstateItem.Area = getValueForKey(tmpJsonResultListEntries, "realEstateEntries."+strconv.Itoa(i)+".attributes.0.attribute.1.value")
					tmpRealEstateItem.Rooms = getValueForKey(tmpJsonResultListEntries, "realEstateEntries."+strconv.Itoa(i)+".attributes.0.attribute.2.value")
					tmpRealEstateItem.Id = getValueForKey(tmpJsonResultListEntries, "realEstateEntries."+strconv.Itoa(i)+".realEstateEntry.@id")
					tmpRealEstateItem.Link = "https://www.immobilienscout24.de/expose/" + tmpRealEstateItem.Id
					tmpRealEstateItem.ImageURL = getImageLink(getIS24ImageLink(tmpJsonResultListEntries, "realEstateEntries."+strconv.Itoa(i)+".realEstateEntry.galleryAttachments.attachment.0.urls.0.url.@href"))
					tmpRealEstateItem.ImageOrigin = "static/IS24.png"

					if mCurrentConfig.DebugLevel == 2 {
						fmt.Println("DEBUG >>> IS24 result list processed data:  " + tmpValue3.String())
						fmt.Println(tmpRealEstateItem.TimeStamp + " | " + tmpRealEstateItem.Price + " | " + tmpRealEstateItem.Location)
						fmt.Println(tmpRealEstateItem.Title + " (" + tmpRealEstateItem.Id + ")")
						fmt.Println(tmpRealEstateItem.Link)
						fmt.Println(tmpRealEstateItem.ImageURL)
						fmt.Println("")
					}

					tmpIsItemNew := isItemNew(tmpRealEstateItem.Id)
					if tmpIsItemNew {
						tmpCounterNewIS24++
					}

					tmpRealEstateItems = append(tmpRealEstateItems, tmpRealEstateItem)
				}

			}

		} else {
			fmt.Println("Error getting HTML Body for IS24.")
		}

		/*
			// Load the HTML document (GOQUERY)
			doc, err := goquery.NewDocumentFromReader(res.Body)
			fmt.Println("Document:", doc)

			if err != nil {
				fmt.Println(err)
			}
		*/

		mRealEstateRepo.ItemsIS24 = nil
		mRealEstateRepo.ItemsIS24 = tmpRealEstateItems
		mRealEstateRepo.NewItemsIS24 = tmpCounterNewIS24
		tmpCounterNewIS24 = 0
		mRealEstateRepo.LastRefreshIS24 = time.Now()
		fmt.Println("Refehed IS24 at: " + mRealEstateRepo.LastRefreshIS24.String())

		go checkAlerts(mRealEstateRepo.ItemsIS24)

	}
}
