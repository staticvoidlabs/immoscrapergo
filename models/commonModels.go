package models

import (
	"time"

	"gorm.io/gorm"
)

// Config struct defines the data model to hold the current configuration given in "config.json".
type Config struct {
	Version         string `json:"version"`
	WebAPIPort      int    `json:"webAPIPort"`
	DebugLevel      int    `json:"debugLevel"`
	RefreshInterval int32  `json:"refreshInterval"`
	UrlEKA          string `json:"urlEKA"`
	UrlIW           string `json:"urlIW"`
	UrlIS24         string `json:"urlIS24"`
	UrlOM           string `json:"urlOM"`
	UrlSAGA         string `json:"urlSAGA"`
	UrlBUWOG        string `json:"urlBUWOG"`
	DeactivateIS24  bool   `json:"deactivateIS24"`
}

type oldNotifyAgent struct {
	JobIndex         int    `json:"jobIndex"`
	IsActive         bool   `json:"isActive"`
	MessageType      string `json:"messageType"`
	SMS77Gateway     string `json:"SMS77Gateway"`
	SMS77APIKey      string `json:"SMS77APIKey"`
	EmailSMTPServer  string `json:"emailSMTPServer"`
	EmailSMTPPort    string `json:"emailSMTPPort"`
	EmailUser        string `json:"emailUser"`
	EmailPass        string `json:"emailPass"`
	RecipientSMS     string `json:"recipientSMS"`
	RecipientEMAIL   string `json:"recipientEMAIL"`
	CriteriaLocation string `json:"criteriaLocation"`
	CriteriaRooms    string `json:"criteriaRooms"`
	CriteriaArea     string `json:"criteriaArea"`
}

type NotifyAgent struct {
	Jobs []NotifyJob `json:"configuredJobs"`
}

type NotifyJob struct {
	IsActive         bool   `json:"isActive"`
	MessageType      string `json:"messageType"`
	SMS77Gateway     string `json:"SMS77Gateway"`
	SMS77APIKey      string `json:"SMS77APIKey"`
	EmailSMTPServer  string `json:"emailSMTPServer"`
	EmailSMTPPort    string `json:"emailSMTPPort"`
	EmailUser        string `json:"emailUser"`
	EmailPass        string `json:"emailPass"`
	RecipientSMS     string `json:"recipientSMS"`
	RecipientEMAIL   string `json:"recipientEMAIL"`
	CriteriaLocation string `json:"criteriaLocation"`
	CriteriaRooms    string `json:"criteriaRooms"`
	CriteriaArea     string `json:"criteriaArea"`
	CriteriaMaxPrice string `json:"criteriaMaxPrice"`
}

type RealEstateItem struct {
	gorm.Model
	Id          string
	TimeStamp   string
	TimeStampDT time.Time
	Title       string
	Price       string
	Location    string
	Rooms       string
	Area        string
	Link        string
	ImageURL    string
	ImageOrigin string
}

type RealEstateRepo struct {
	ItemsEKA         []RealEstateItem
	ItemsIW          []RealEstateItem
	ItemsIS24        []RealEstateItem
	ItemsOM          []RealEstateItem
	ItemsSAGA        []RealEstateItem
	ItemsBUWOG       []RealEstateItem
	NewItemsEKA      int
	NewItemsIW       int
	NewItemsIS24     int
	NewItemsOM       int
	NewItemsSAGA     int
	NewItemsBUWOG    int
	LastRefreshEKA   time.Time
	LastRefreshIW    time.Time
	LastRefreshIS24  time.Time
	LastRefreshOM    time.Time
	LastRefreshSAGA  time.Time
	LastRefreshBUWOG time.Time
	LastWebAccess    time.Time
}

type ViewModelMainSite struct {
	SiteTitle             string
	RealEstatesEKA        []RealEstateItem
	RealEstatesIW         []RealEstateItem
	RealEstatesIS24       []RealEstateItem
	RealEstatesOM         []RealEstateItem
	RealEstatesSAGA       []RealEstateItem
	RealEstatesBUWOG      []RealEstateItem
	NewItemsEKA           int
	NewItemsIW            int
	NewItemsIS24          int
	NewItemsOM            int
	NewItemsSAGA          int
	NewItemsBUWOG         int
	RefreshedAtEKA        time.Time
	RefreshedAtIW         time.Time
	RefreshedAtIS24       time.Time
	RefreshedAtOM         time.Time
	RefreshedAtSAGA       time.Time
	RefreshedAtBUWOG      time.Time
	ApartmentRequestText  string
	ApartmentRequestText2 string
}
