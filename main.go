package main

import (
	"math/rand"
	"time"

	"gitlab.com/staticvoidlabs/immoscrapergo/managers"
)

func main() {

	managers.ProcessConfigFile()
	managers.ProcessNotifyAgentFile()
	managers.InitPostalCodeMap()

	go managers.InitAPIService()

	for {

		managers.StartSearchLoop()

		tmpRefrehInterval := managers.GetRefrehInterval()
		time.Sleep(time.Duration(rand.Int31n(tmpRefrehInterval)) * time.Second)

		if managers.ShouldExit {
			break
		}
	}

}
