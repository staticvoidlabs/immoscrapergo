module gitlab.com/staticvoidlabs/immoscrapergo

go 1.17

require github.com/gorilla/mux v1.8.0

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/andybalholm/cascadia v1.3.1 // indirect
	golang.org/x/net v0.0.0-20220425223048-2871e0cb64e4 // indirect
	gorm.io/gorm v1.23.5
)

require (
	github.com/dghubble/oauth1 v0.7.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/tidwall/gjson v1.14.1 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
)
